package com.example.philipp.motoloc;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;//client de l'api play pour la loc
    private Location mLastLocation;//variale de stockage des infos de position du client
    private LocationRequest mLocationRequest;
    private String mInternetResponse;
    public TextView mTextView;
    private JSONObject mjsonServerResponse;
    private ArrayList<HashMap<String, String>> contactList = new ArrayList<>();//gere la liste d'utilisateurs de la requete json
    private final int REQUEST_PERMISSION_GPS=1;//permet le callback de la gestion de permission

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final SharedPreferences settings = getSharedPreferences("MotoLocPrefs",0);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Connection au serveur", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //upload position to server
                getInternetInfo();
            }
        });

        final Button departure = (Button) findViewById(R.id.button2);//gère le bouton je pars, et active le service de fond si nécessaire
        if(settings.getBoolean("service",false)){
            departure.setText("Je suis parti");
        }
        else
        {
            departure.setText("Je pars");
        }
        departure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                if(settings.getBoolean("service",false)){
                    stopService(new Intent(getApplicationContext(), Background.class));
                    editor.putBoolean("service",false);
                    departure.setText("Je pars");
                }
                else
                {
                    //demarre le service de fond
                    startService(new Intent(getApplicationContext(), Background.class));
                    editor.putBoolean("service",true);
                    departure.setText("Je suis arrivé");
                }
                editor.commit();

            }
        });

        final Button save = (Button) findViewById(R.id.saveBtn);//gère le bouton je pars, et active le service de fond si nécessaire
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences settings = getSharedPreferences("MotoLocPrefs",0);
                SharedPreferences.Editor editor = settings.edit();
                EditText user = (EditText) findViewById(R.id.editText);
                EditText ip = (EditText) findViewById(R.id.edit_ip);
                editor.putString("username", user.getText().toString());
                editor.putString("ip", ip.getText().toString());
                editor.commit();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //connection au service google pour la localisation
        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }

        //on récupère les réglages

        EditText username = (EditText) findViewById(R.id.editText);
        username.setText(settings.getString("username","").toString());
        EditText ip = (EditText) findViewById(R.id.edit_ip);
        ip.setText(settings.getString("ip","").toString());



        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    protected void onStart() {//fonction appelee au demarrage
        mGoogleApiClient.connect();
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.start(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    protected void onStop() {//on se déconnect de l'api lors de la fermeture de l'app
        mGoogleApiClient.disconnect();
        super.onStop();// ATTENTION: This was auto-generated to implement the App Indexing API.
// See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mGoogleApiClient, getIndexApiAction());
        SharedPreferences settings = getSharedPreferences("MotoLocPrefs",0);
        SharedPreferences.Editor editor = settings.edit();
        EditText user = (EditText) findViewById(R.id.editText);
        EditText ip = (EditText) findViewById(R.id.edit_ip);
        editor.putString("username", user.getText().toString());
        editor.putString("ip", ip.getText().toString());
        editor.commit();
    }

    @Override
    protected void onPause(){//on met en pause les update de localisation lors de la mise en fond
        super.onPause();
        if(mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    protected void onResume(){//on reprend la mise a jour de la loc lors du passage au premier plan de l'app
        super.onResume();
        //on vérifie les permissions
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                showExplanation("Autoriser la localisation", "La localisation est nécessaire pour cette application", android.Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_GPS);
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_GPS);
            }
        } else {
            //Toast.makeText(MainActivity.this, "Permission (already) Granted!", Toast.LENGTH_SHORT).show();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                updateUI();
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void createLocationRequest(){//cree la demande de localisation pour la première fois
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);//toutes les 10 secondes
        mLocationRequest.setFastestInterval(5000);//au plus court toutes les 5 secondes une update
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//on veut le gps pour une loc précise au détriment de la batterie
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_map) {
            EditText user = (EditText) findViewById(R.id.editText);
            EditText ip = (EditText) findViewById(R.id.edit_ip);
            if(ip.equals("") || user.equals("")){
                Snackbar.make(findViewById(R.id.content_main), "Veuillez remplir les champs nom et ip", Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
            else {
                Intent intent = new Intent(this, MapsActivity.class);
                //envoie des coordonnes personnelles et des contacts
                SharedPreferences settings = getSharedPreferences("MotoLocPrefs", 0);
                SharedPreferences.Editor editor = settings.edit();

                editor.putString("username", user.getText().toString());
                editor.putString("ip", ip.getText().toString());
                editor.commit();

                intent.putExtra("contacts", contactList);
                intent.putExtra("location", mLastLocation);
                startActivity(intent);
            }
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //lors de la connection a l'api, on demande la dernière localisation pour un affichage direct.
        //on vérifie les permissions
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                showExplanation("Permission Needed", "Rationale", android.Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_GPS);
            } else {
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_PERMISSION_GPS);
            }
        } else {
            //Toast.makeText(MainActivity.this, "Permission (already) Granted!", Toast.LENGTH_SHORT).show();
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                updateUI();
            }
            createLocationRequest();
            LocationSettingsRequest.Builder builder =  new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);//on demande les mises a jour de la localisation régulière
        }


        //Snackbar.make(findViewById(R.id.content_main), "Connected to google api", Snackbar.LENGTH_LONG).setAction("Action", null).show();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Snackbar.make(findViewById(R.id.content_main), "Connection failed to the google api", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        //Snackbar.make(findViewById(R.id.content_main), "Location updated", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        updateUI();
    }

    public void updateUI(){
        TextView latitudeText = (TextView) findViewById(R.id.latitudeText);
        TextView longitudeText = (TextView) findViewById(R.id.longitudeText);
        TextView timeText = (TextView) findViewById(R.id.timeText);
        latitudeText.setText(String.valueOf(mLastLocation.getLatitude()));
        timeText.setText(String.valueOf(mLastLocation.getTime()));
        longitudeText.setText(String.valueOf(mLastLocation.getLongitude()));
    }

    private void getInternetInfo(){
        //recupere la liste des autres utilisateurs sur le serveur

        // Instantiate the RequestQueue.
        mTextView = (TextView) findViewById(R.id.internetText);
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://ftmarshmallow.com/json.php";
        //on recupere un objet json du serveur
        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //mjsonServerResponse = response.;//on sauvegarde dans une variable de classe la liste
                        try {
                            Log.i("JsonResponse", response.toString());
                            //JSONArray users = response.getJSONArray(0);//.getJSONArray("users"); <- old
                            Log.i("JsonArray",response.toString());
                            for(int i = 0;i<response.length();i++){
                                JSONObject c = response.getJSONObject(i);
                                HashMap<String, String> contact = new HashMap<>();
                                contact.put("name",c.getString("user"));
                                contact.put("lat", c.getString("lat"));
                                contact.put("lon", c.getString("lon"));
                                contactList.add(contact);
                                mTextView.setText(response.length() + " users found " );
                            }
                        }
                        catch (JSONException e){//en cas d'erreur de parsing on log l'erreur et on informe l'utilisateur de l'erreur
                            Snackbar.make(findViewById(R.id.content_main), "Une erreur est survenue", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            Log.e("Json",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(findViewById(R.id.content_main), "Connection au serveur impossible", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Log.e("Volley",error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(jsObjRequest);
    }


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_GPS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Toast.makeText(this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                } else {
                    //Toast.makeText(this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void showExplanation(String title,
                                 String message,
                                 final String permission,
                                 final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestPermission(permission, permissionRequestCode);
                    }
                });
        builder.create().show();
    }

    private void requestPermission(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionName}, permissionRequestCode);
    }
}
