package com.example.philipp.motoloc;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private Location mLastLocation;//on stock la derniere position recue
    private ArrayList<HashMap<String, String>>  contacts = new ArrayList<>();
    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private JSONObject mjsonServerResponse;
    private ArrayList<HashMap<String, String>> contactList = new ArrayList<>();//gere la liste d'utilisateurs de la requete json
    private GoogleApiClient mGoogleApiClient;//client de l'api play pour la loc
    private LocationRequest mLocationRequest;
    private final int REQUEST_PERMISSION_GPS=1;//permet le callback de la gestion de permission

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        retrieveExtra();//recupère les infos de la première activité

        mHandler = new Handler();//Gestion de la tache de fond pour recuperer les infos des contacts
        mStatusChecker.run();

        //connection au service google pour la localisation
        if (mGoogleApiClient == null) {
            // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
            // See https://g.co/AppIndexing/AndroidStudio for more information.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(AppIndex.API).build();
        }
    }
    @Override
    protected void onStart() {//fonction appelee au demarrage
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {//on se déconnect de l'api lors de la fermeture de l'app
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onPause(){//on met en pause les update de localisation lors de la mise en font
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);

    }

    @Override
    protected void onResume(){//on reprend la mise a jour de la loc lors du passage au premier plan de l'app
        super.onResume();
        if(mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);
    }

    protected void createLocationRequest(){//cree la demande de localisation pour la première fois
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);//toutes les 10 secondes
        mLocationRequest.setFastestInterval(5000);//au plus court toutes les 5 secondes une update
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);//on veut le gps pour une loc précise au détriment de la batterie
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {//lors de la connection a l'api, on demande la dernière localisation pour un affichage direct.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            updateUI(false);
        }
        createLocationRequest();
        LocationSettingsRequest.Builder builder =  new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);//on demande les mises a jour de la localisation régulière

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Snackbar.make(findViewById(R.id.content_main), "Connection failed to the google api", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();//erreur de connection a l'api google.
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;//on met a jour la position actuelle
        updateUI(false);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        updateUI(true);
    }

    public void updateUI(boolean firstUpdate){//met a jour l'affichage

        //on recupere le nom choisis par l'utilisateur
        SharedPreferences settings = getSharedPreferences("MotoLocPrefs",0);
        String username = settings.getString("username","User");

        if (mLastLocation != null && mMap != null) {
            mMap.clear();//on efface tout les marqueurs de la map

            for(int i = 0;i<contacts.size();i++){//ajoute les markers des contacts sur la carte
                if(!contacts.get(i).get("name").equals(username)) {//on affiche pas notre coord en plus
                    LatLng contact = new LatLng(Double.parseDouble(contacts.get(i).get("lat")), Double.parseDouble(contacts.get(i).get("lon")));
                    mMap.addMarker(new MarkerOptions().position(contact).title(contacts.get(i).get("name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                }
            }

            LatLng user = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());//on cree le marqueur de l'utilisateur
            mMap.addMarker(new MarkerOptions().position(user).title(username));
            if(firstUpdate){//si c'est la première ouverture de la carte de la session, on zoome sur la position de l'utilisateur
                mMap.moveCamera(CameraUpdateFactory.newLatLng(user));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
            }


        }
    }

    public void retrieveExtra(){//récupère les infos de première localisation de la première page
        Intent intent = getIntent();
        mLastLocation = intent.getExtras().getParcelable("location");
        contacts = (ArrayList<HashMap<String, String>>) intent.getSerializableExtra("contacts");
    }

    public void onDestroy(){
        mHandler.removeCallbacks(mStatusChecker);
        super.onDestroy();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                getInternetInfo();
                updateUI(false);
            } finally {
                // 100% guarantee that this always happens, even if
                // your update method throws an exception
                mHandler.postDelayed(mStatusChecker, mInterval);
            }
        }
    };

    private void getInternetInfo(){
        //recupere la liste des autres utilisateurs sur le serveur
        SharedPreferences settings = getSharedPreferences("MotoLocPrefs",0);
        final String username = settings.getString("username","User");
        final String ip = settings.getString("ip","");

        // Instantiate the RequestQueue.
        //mTextView = (TextView) findViewById(R.id.internetText);
        RequestQueue queue = Volley.newRequestQueue(this);

        //on recupere un objet json du serveur
        JsonArrayRequest jsObjRequest = new JsonArrayRequest
                (Request.Method.POST, ip, null, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        contactList.clear();
                        try {
                            for(int i = 0;i<response.length();i++){
                                JSONObject c = response.getJSONObject(i);
                                HashMap<String, String> contact = new HashMap<>();
                                contact.put("name",c.getString("user"));//on stock le nom
                                contact.put("lat", c.getString("lat"));//la latitude
                                contact.put("lon", c.getString("lon"));//et la longiture des contacts
                                contactList.add(contact);
                            }
                            contacts = contactList;
                        }
                        catch (JSONException e){//en cas d'erreur de parsing on log l'erreur et on informe l'utilisateur de l'erreur
                            //Snackbar.make(findViewById(R.id.content_main), "Une erreur est survenue", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                            Log.e("Json",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Snackbar.make(findViewById(R.id.content_main), "Connection au serveur impossible", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                        Log.e("Volley",error.toString());
                    }
                });
// Add the request to the RequestQueue.
        queue.add(jsObjRequest);
        //envoi de la localisation
        StringRequest postRequest = new StringRequest(Request.Method.POST, ip,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user", username);
                params.put("lat", String.valueOf(mLastLocation.getLatitude()));
                params.put("lon", String.valueOf(mLastLocation.getLongitude()));

                return params;
            }
        };
        queue.add(postRequest);
    }



}
